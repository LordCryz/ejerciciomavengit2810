package com.softtek.course;

public class Calculator {
    double a;
    double b;

    public double sum() {
        return a + b;
    }

    public double mult() {
        return a * b;
    }

    public double res() {
        return a - b;
    }

    public double div(){
        return a / b;
    }

}
